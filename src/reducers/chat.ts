import { AnyAction } from "redux";
import {
  LOAD_MESSAGES,
  DELETE_MESSAGE,
  ADD_MESSAGE,
  SET_EDIT_MODAL,
  SET_EDITING_MESSAGE,
  EDIT_MESSAGE,
  ON_EDIT_LAST_MESSAGE,
} from "../actions/types";
import { MessageType } from "../Types/MessageType";

export type ChatStoreType = {
  messages: MessageType[];
  editModal: boolean;
  preloader: boolean;
  userId: string;
  user: string;
  editingMessage: MessageType;
  inputText: string;
  editPostId: string;
};

const initialState: ChatStoreType = {
  messages: [],
  editModal: false,
  preloader: true,
  userId: "4b003c20-1b8f-11e8-9629-c7eca82aa7bd",
  user: "Helen",
  editingMessage: {
    avatar: "",
    createdAt: "",
    editedAt: "",
    id: "",
    text: "",
    user: "",
    userId: "",
  },
  inputText: "",
  editPostId: "",
};

export default function chat(state = initialState, action: AnyAction) {
  switch (action.type) {
    case LOAD_MESSAGES: {
      const newState = { ...state };
      newState.messages = [...action.payload];
      newState.preloader = false;
      return newState;
    }
    case DELETE_MESSAGE: {
      const id = action.payload;
      const messages = state.messages.filter(message => message.id !== id);
      return { ...state, messages };
    }
    case ADD_MESSAGE: {
      const message = action.payload;
      const messages = [...state.messages];
      messages.push(message);
      return { ...state, messages };
    }
    case SET_EDIT_MODAL: {
      const editModal = action.payload;
      return { ...state, editModal };
    }
    case SET_EDITING_MESSAGE: {
      const editingMessage = { ...action.payload };
      return { ...state, editingMessage };
    }
    case EDIT_MESSAGE: {
      const newMessage = action.payload;
      newMessage.editedAt = new Date(Date.now()).toString();
      const messages = state.messages.map(message => {
        return message.id === newMessage.id ? newMessage : message;
      });
      return { ...state, messages };
    }
    case ON_EDIT_LAST_MESSAGE: {
      const { userId } = state;
      const userMessages = state.messages.filter(
        message => message.userId === userId
      );

      if (!userMessages) {
        return { ...state };
      }

      const editingMessage = userMessages[userMessages.length - 1];

      const newState = { ...state };
      newState.editModal = true;
      newState.editingMessage = editingMessage;
      return { ...newState };
    }
    default:
      return state;
  }
}
