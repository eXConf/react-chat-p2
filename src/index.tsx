import React from "react";
import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import store from "./store";
import Chat from "./Components/Chat";
import "./global.css";
import "./reset.css";

ReactDOM.render(
  <React.StrictMode>
    <Provider store={store}>
      <Chat
        url={"https://edikdolynskyi.github.io/react_sources/messages.json"}
      />
    </Provider>
  </React.StrictMode>,
  document.getElementById("root")
);
