import {
  LOAD_MESSAGES,
  DELETE_MESSAGE,
  ADD_MESSAGE,
  SET_EDIT_MODAL,
  SET_EDITING_MESSAGE,
  EDIT_MESSAGE,
  ON_EDIT_LAST_MESSAGE,
} from "./types";
import { MessageType } from "../Types/MessageType";

export const loadMessages = (messages: MessageType[]) => ({
  type: LOAD_MESSAGES,
  payload: messages,
});

export const deleteMessage = (id: string) => ({
  type: DELETE_MESSAGE,
  payload: id,
});

export const addMessage = (message: MessageType) => ({
  type: ADD_MESSAGE,
  payload: message,
});

export const setEditModal = (bool: boolean) => ({
  type: SET_EDIT_MODAL,
  payload: bool,
});

export const setEditingMessage = (message: MessageType) => ({
  type: SET_EDITING_MESSAGE,
  payload: message,
});

export const editMessage = (message: MessageType) => ({
  type: EDIT_MESSAGE,
  payload: message,
});

export const onEditLastMessage = () => ({
  type: ON_EDIT_LAST_MESSAGE,
});
