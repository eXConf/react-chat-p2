import React, { Component } from "react";
import "./header.css";

type HeaderProps = {
  usersCount: number;
  messagesCount: number;
  lastMessageTime: Date | null;
};

export default class Header extends Component<HeaderProps> {
  render() {
    return (
      <div className="header">
        <div className="header-left">
          <div className="header-title">MyChat</div>
          <div className="header-users-count">{this.props.usersCount}</div>
          <div className="header-messages-count">
            {this.props.messagesCount}
          </div>
        </div>
        <div className="header-right">
          <div className="header-last-message-date">
            {this.formatDateTime(this.props.lastMessageTime)}
          </div>
        </div>
      </div>
    );
  }

  formatDateTime(date: Date | null): String {
    if (date === null) return "never";

    const dateRU = new Intl.DateTimeFormat("ru-RU", {
      dateStyle: "short",
      timeStyle: "short",
    }).format(date);
    const dateWithoutComma = dateRU.replace(",", "");

    return dateWithoutComma;
  }
}
