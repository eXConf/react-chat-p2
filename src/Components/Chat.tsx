import React, { Component } from "react";
import Header from "./Header/Header";
import MessageList from "./MessageList/MessageList";
import MessageInput from "./MessageInput/MessageInput";
import { MessageType } from "../Types/MessageType";
import Preloader from "./Preloader/Preloader";
import EditMessage from "./EditMessage/EditMessage";
import "./chat.css";
import { connect } from "react-redux";
import { ChatStoreType } from "../reducers/chat";
import * as actions from "../actions";

interface ComponentOwnProps {
  url: string;
}

type ComponentMapProps = ReturnType<typeof mapStateToProps> &
  typeof mapDispatchToProps;

type ComponentProps = ComponentMapProps & ComponentOwnProps;

class Chat extends Component<ComponentProps> {
  async componentDidMount(): Promise<void> {
    const data = await this.fetchData(this.props.url);
    const messages = this.sortMessagesByDate(data);

    this.props.loadMessages(messages);
  }

  render() {
    const { messages, userId } = this.props.chat;

    return this.props.chat.preloader ? (
      <Preloader />
    ) : (
      <div className="chat" style={{ margin: "20px 50px" }}>
        {this.props.chat.editModal ? (
          <EditMessage
            message={this.props.chat.editingMessage}
            isOpen={this.props.chat.editModal}
          />
        ) : (
          ""
        )}
        <Header
          usersCount={this.getUsersCount()}
          messagesCount={this.getMessagesCount()}
          lastMessageTime={this.getLastMessageTime()}
        />
        <MessageList messages={messages} userId={userId} />
        <MessageInput />
      </div>
    );
  }

  async fetchData(url: string): Promise<MessageType[]> {
    const response = await fetch(url);
    const data: MessageType[] = await response.json();

    return data;
  }

  getUsersCount(): number {
    const { messages } = this.props.chat;
    const users = messages.map((message: MessageType) => message.userId);
    const unique = new Set(users);

    return unique.size;
  }

  getMessagesCount(): number {
    const { messages } = this.props.chat;
    return messages.length;
  }

  getLastMessageTime(): Date | null {
    const { messages }: { messages: MessageType[] } = this.props.chat;

    if (messages.length === 0) return null;

    const time = messages[messages.length - 1].createdAt;
    return new Date(time);
  }

  sortMessagesByDate(data: MessageType[]): MessageType[] {
    const messages = [...data];

    messages.sort((a: MessageType, b: MessageType) => {
      return new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime();
    });

    return messages;
  }
}

type StateProps = {
  chat: ChatStoreType;
};

const mapStateToProps = (state: StateProps) => {
  return {
    chat: state.chat,
  };
};

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(Chat);
