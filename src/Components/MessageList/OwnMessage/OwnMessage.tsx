import React, { Component } from "react";
import { MessageType } from "../../../Types/MessageType";
import { getHoursMinutes } from "../../../helpers/dateTimeHelper";
import { connect } from "react-redux";
import * as actions from "../../../actions";
import { ChatStoreType } from "../../../reducers/chat";
import "./own-message.css";

interface ComponentOwnProps {
  message: MessageType;
}

type ComponentMapProps = ReturnType<typeof mapStateToProps> &
  typeof mapDispatchToProps;

type ComponentProps = ComponentMapProps & ComponentOwnProps;

class OwnMessage extends Component<ComponentProps> {
  render() {
    const { createdAt, editedAt, text, id } = this.props.message;
    const time = editedAt ? editedAt : createdAt;
    const timeFormatted = getHoursMinutes(new Date(time));

    return (
      <div className="own-message">
        <div className="text-container">
          <div className="message-info">
            <div className="message-time">{timeFormatted}</div>
            <div className="controls">
              <div
                className="message-edit"
                onClick={() => this.onEditClicked()}
              >
                <i className="fas fa-pencil-alt"></i>
              </div>
              <div
                className="message-delete"
                onClick={() => this.onDeleteClicked(id)}
              >
                <i className="fas fa-trash-alt"></i>
              </div>
            </div>
          </div>
          <div className="message-text">{text}</div>
        </div>
      </div>
    );
  }

  onEditClicked() {
    const { message } = this.props;
    this.props.setEditingMessage(message);
    this.props.setEditModal(true);
  }

  onDeleteClicked(id: string) {
    this.props.deleteMessage(id);
  }
}

type StateProps = {
  chat: ChatStoreType;
};

const mapStateToProps = (state: StateProps) => {
  return {
    chat: state.chat,
  };
};

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(OwnMessage);
