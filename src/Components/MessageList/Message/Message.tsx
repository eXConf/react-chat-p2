import React, { Component } from "react";
import { MessageType } from "../../../Types/MessageType";
import { getHoursMinutes } from "../../../helpers/dateTimeHelper";
import "./message.css";

type MessageProps = {
  message: MessageType;
};

export default class Message extends Component<MessageProps> {
  likeRef = React.createRef<HTMLDivElement>();

  render() {
    const { createdAt, editedAt, avatar, user, text } = this.props.message;
    const time = editedAt ? editedAt : createdAt;
    const timeFormatted = getHoursMinutes(new Date(time));

    return (
      <div className="message">
        <img src={avatar} className="message-user-avatar" alt={user} />
        <div className="text-container">
          <div className="message-info">
            <div className="message-user-name">{user}</div>
            <div className="message-time">{timeFormatted}</div>
          </div>
          <div className="message-text">{text}</div>
        </div>
        <div
          className="message-like"
          ref={this.likeRef}
          onClick={() => this.onLikeClicked()}
        >
          <i className="fas fa-heart"></i>
        </div>
      </div>
    );
  }

  onLikeClicked() {
    const div = this.likeRef.current;
    if (div?.classList.contains("message-like")) {
      div.classList.replace("message-like", "message-liked");
    } else {
      div?.classList.replace("message-liked", "message-like");
    }
  }
}
