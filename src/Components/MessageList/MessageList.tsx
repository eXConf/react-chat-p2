import React, { Component } from "react";
import { MessageType } from "../../Types/MessageType";
import Message from "./Message/Message";
import OwnMessage from "./OwnMessage/OwnMessage";
import "./message-list.css";

type MessageListProps = {
  messages: MessageType[];
  userId: string;
};

export default class MessageList extends Component<MessageListProps> {
  render() {
    return (
      <div className="message-list">
        {this.props.messages.map((msg, idx, arr) => {
          const dateFarInThePast = new Date("01/01/1000");

          return (
            <React.Fragment key={msg.createdAt}>
              {idx === 0
                ? this.renderDivider(
                    dateFarInThePast,
                    new Date(arr[idx].createdAt)
                  )
                : this.renderDivider(
                    new Date(arr[idx - 1].createdAt),
                    new Date(arr[idx].createdAt)
                  )}
              {this.props.userId === msg.userId ? (
                <OwnMessage key={msg.id} message={msg} />
              ) : (
                <Message key={msg.id} message={msg} />
              )}
            </React.Fragment>
          );
        })}
      </div>
    );
  }

  renderDivider(prevDate: Date, curDate: Date) {
    if (this.isSameDay(prevDate, curDate)) {
      return;
    }

    return <div className="messages-divider">{this.formatDate(curDate)}</div>;
  }

  formatDate(msgDate: Date): String {
    const today = new Date(Date.now());
    const msgDatePlus = new Date(msgDate);
    msgDatePlus.setDate(msgDatePlus.getDate() + 1);

    if (this.isSameDay(msgDate, today)) {
      return "Today";
    }

    if (this.isSameDay(msgDatePlus, today)) {
      return "Yesterday";
    }

    const formattedDate = new Intl.DateTimeFormat("en-GB", {
      weekday: "long",
      day: "numeric",
      month: "long",
    }).format(msgDate);

    return formattedDate;
  }

  isSameDay(date1: Date, date2: Date): Boolean {
    if (
      date1.getDate() === date2.getDate() &&
      date1.getMonth() === date2.getMonth() &&
      date1.getFullYear() === date2.getFullYear()
    ) {
      return true;
    }
    return false;
  }
}
