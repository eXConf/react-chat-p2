import React, { Component } from "react";
import { MessageType } from "../../Types/MessageType";
import { connect } from "react-redux";
import { ChatStoreType } from "../../reducers/chat";
import * as actions from "../../actions";
import "./message-input.css";

type ComponentMapProps = ReturnType<typeof mapStateToProps> &
  typeof mapDispatchToProps;

type ComponentProps = ComponentMapProps;

interface ComponentState {
  inputText: string;
}

class MessageInput extends Component<ComponentProps> {
  state: ComponentState = {
    inputText: "",
  };

  inputRef = React.createRef<HTMLInputElement>();

  render() {
    return (
      <div className="message-input">
        <input
          value={this.state.inputText}
          onChange={e =>
            this.setState(prev => ({ ...prev, inputText: e.target.value }))
          }
          onKeyUp={e => this.onInputKeyKeyUp(e)}
          ref={this.inputRef}
          type="text"
          className="message-input-text"
        />
        <button
          onClick={() => this.postMessage()}
          className="message-input-button"
        >
          Send
        </button>
      </div>
    );
  }

  onInputKeyKeyUp(e: React.KeyboardEvent) {
    if (e.key === "Enter") {
      return this.postMessage();
    }
    if (e.key === "ArrowUp" && this.state.inputText === "") {
      this.props.onEditLastMessage();
    }
  }

  postMessage() {
    if (!this.state.inputText) return;

    const { user, userId } = this.props.chat;
    const message: MessageType = {
      avatar: "",
      createdAt: new Date().toString(),
      editedAt: "",
      id: Date.now().toString(),
      text: this.state.inputText,
      user: user,
      userId: userId,
    };

    this.props.addMessage(message);
    this.inputRef.current?.focus();
    this.setState(
      prev => ({ ...prev, inputText: "" }),

      () => {
        if (window) {
          window?.scrollTo(0, document.body.scrollHeight);
        }
      }
    );
  }
}

type StateProps = {
  chat: ChatStoreType;
};

const mapStateToProps = (state: StateProps) => {
  return {
    chat: state.chat,
  };
};

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(MessageInput);
