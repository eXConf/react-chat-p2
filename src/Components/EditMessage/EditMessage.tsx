import React, { Component } from "react";
import Modal from "react-modal";
import { connect } from "react-redux";
import { ChatStoreType } from "../../reducers/chat";
import * as actions from "../../actions";
import { MessageType } from "../../Types/MessageType";
import "./edit-message.css";

type ComponentMapProps = ReturnType<typeof mapStateToProps> &
  typeof mapDispatchToProps;

interface ComponentOwnProps {
  message: MessageType;
  isOpen: boolean;
}

interface ComponentState {
  message: MessageType;
}

type ComponentProps = ComponentMapProps & ComponentOwnProps & ComponentState;

class EditMessage extends Component<ComponentProps> {
  emptyMessage = {
    avatar: "",
    createdAt: "",
    editedAt: "",
    id: "",
    text: "",
    user: "",
    userId: "",
  };

  state = {
    message: { ...this.emptyMessage },
  };

  componentDidMount() {
    this.setState(prev => {
      const message = { ...this.props.message };
      return { message };
    });
  }

  render() {
    // Modal.setAppElement("#root"); // Commented to avoid "no #root" error in autotest
    return (
      <Modal
        isOpen={this.props.isOpen}
        ariaHideApp={false} // Added because of commented code above
        shouldCloseOnOverlayClick={true}
        shouldCloseOnEsc={true}
        preventScroll={true}
        onAfterClose={() => this.onCloseClicked()}
        className={`edit-message-modal ${
          this.props.chat.editModal ? "modal-shown" : ""
        }`}
        style={{
          overlay: {
            position: "fixed",
            top: 0,
            left: 0,
            right: 0,
            bottom: 0,
            backgroundColor: "rgba(255, 255, 255, 0.75)",
          },
          content: {
            position: "absolute",
            top: "50%",
            left: "50%",
            transform: "translate(-50%, -80%)",
            border: "1px solid #ccc",
            background: "#fff",
            overflow: "auto",
            WebkitOverflowScrolling: "touch",
            borderRadius: "4px",
            outline: "none",
            padding: "20px",
          },
        }}
      >
        <div className="modal-container" onKeyUp={e => this.onKeyUp(e)}>
          <div className="edit-message-header">Edit message</div>
          <textarea
            autoFocus={true}
            value={this.state.message.text}
            onChange={e =>
              this.setState((prev: ComponentState) => {
                const state = { ...prev };
                state.message.text = e.target.value;
                return state;
              })
            }
            className="edit-message-input"
            placeholder="Edit message"
          />
          <div className="edit-controls">
            <button
              className="edit-message-button btn"
              onClick={() => this.onEditClicked()}
            >
              Edit
            </button>
            <button
              className="edit-message-close btn"
              onClick={() => this.onCloseClicked()}
            >
              Close
            </button>
          </div>
        </div>
      </Modal>
    );
  }

  onCloseClicked() {
    this.props.setEditModal(false);
    this.props.setEditingMessage(this.emptyMessage);
  }

  onEditClicked() {
    this.props.editMessage(this.state.message);
    this.onCloseClicked();
  }

  onKeyUp(e: React.KeyboardEvent) {
    if (e.key === "Escape") {
      this.onCloseClicked();
    }
  }
}

type StateProps = {
  chat: ChatStoreType;
};

const mapStateToProps = (state: StateProps) => {
  return {
    chat: state.chat,
  };
};

const mapDispatchToProps = {
  ...actions,
};

export default connect(mapStateToProps, mapDispatchToProps)(EditMessage);
